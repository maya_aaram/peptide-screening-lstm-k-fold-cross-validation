# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function, division
from io import open
import unicodedata
import string
import re
import random
from datetime import datetime
from pathlib import Path
import os
import glob

import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F
from torch.utils.data import TensorDataset , Dataset, DataLoader
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import pandas as pd
import numpy as np
from sklearn.metrics import r2_score , mean_squared_error
import matplotlib.pyplot as plt

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

######################################################################################################
######################################################################################################
class AAs:
    def __init__(self, name, sequences):
        # The constructor expects a name and a list of strings.
        self.name = name
        self.aa2index = {}
        self.aa2count = {}
        self.index2aa = {}
        self.n_residues = 0  # start as empty dict
        #print("Counting amino acids ...")
        for seq in sequences:
            self.addSequence(seq)

    def addSequence(self, sequence):
        for aa in list(sequence.upper()):
            self.addResidue(aa)

    def addResidue(self, aa):
        if aa not in self.aa2index:
            self.aa2index[aa] = self.n_residues + 1
            self.aa2count[aa] = 1
            self.index2aa[self.n_residues] = aa
            self.n_residues += 1
        else:
            self.aa2count[aa] += 1
    
    def FASTA2indices(self, seq, max_length = 70):
        indices = [0] * max_length
        try:
            for i, aa in enumerate(list(seq.upper())):
                indices[i] = self.aa2index[aa]
            return indices
        except:
            #print(f"Something didn't work when converting {seq} to indices.")
            return None
######################################################################################################
######################################################################################################
class PepDS(Dataset):
    def __init__(self, X, Y, l):
        self.X = X
        self.y = Y
        self.l = l
        
    def __len__(self):
        return len(self.y)
    
    def __getitem__(self, idx):
        return self.X[idx], self.y[idx], self.l[idx]
######################################################################################################
######################################################################################################
class LSTM_regr(torch.nn.Module) :
    def __init__(self, vocab_size, embedding_dim, hidden_dim1, hidden_dim2, num_layers=1, dr_o = 0.1, batch_first=True ) :
        super().__init__()
        self.embeddings = nn.Embedding(vocab_size, embedding_dim, padding_idx=0, max_norm = 1)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim1, batch_first=True)
        self.dropout = nn.Dropout(dr_o)
        self.linear1 = nn.Linear(hidden_dim1, hidden_dim2)
        self.linear2 = nn.Linear(hidden_dim2, 1)

        
    def forward(self, x, l):
        x1 = self.embeddings(x)
        #print("\n\n",x, "\n#################\n", x1)
        x = self.dropout(x1)
        x_pack = pack_padded_sequence(x, l, batch_first=True, enforce_sorted=False)
        lstm_out, (ht, ct) = self.lstm(x_pack)
        #xx= F.relu(self.linear1(ht[-1]))
        xx= F.leaky_relu(self.linear1(ht[-1]))
        #xx = self.dropout(xx)
        return self.linear2(xx)
######################################################################################################
######################################################################################################
######################################################################################################
######################################################################################################
def prepareData(filename):
    pairs = pd.read_csv(filename)
    AA = AAs("Amino Acids", pairs["FASTA"].tolist())
    #print("Counted amino acids:", AA.name, AA.n_residues)
    return AA, pairs
######################################################################################################
######################################################################################################
def train_k_fold(data, num_layers , embedding_dim, hidden_dim1 , hidden_dim2, dr_o, lr=0.01, epochs = 1000, k = 8, batch_size = 10):
    results = []
    test_data = np.array_split(data, k)
    for i in range(k):
        model =  LSTM_regr(vocab_size = 21, num_layers = 1, embedding_dim = 38, hidden_dim1 = 16, hidden_dim2 = 37, dr_o = 0.5)
        model_summary = str(repr(model))
        #print(f"Fold {i+1}")
        train_val = data[~data.index.isin(test_data[i].index)]
        train_data = train_val.sample(frac=0.90, axis = 0, random_state = 88)
        val_data = train_val[~train_val.index.isin(train_data.index)]
        
        train_data.reset_index(inplace = True, drop = True)
        val_data.reset_index(inplace = True, drop = True)
        test_data[i].reset_index(inplace = True, drop = True)

        X_train = torch.tensor(list(train_data["encoded_FASTA"]))
        Y_train = torch.tensor(list(train_data["MIC_against_Salmonella(µM)"]))
        l_train = torch.tensor(list(train_data["length"]))

        X_val = torch.tensor(list(val_data["encoded_FASTA"]))
        Y_val = torch.tensor(list(val_data["MIC_against_Salmonella(µM)"]))
        l_val = torch.tensor(list(val_data["length"]))

        X_test = torch.tensor(list(test_data[i]["encoded_FASTA"]))
        Y_test = torch.tensor(list(test_data[i]["MIC_against_Salmonella(µM)"]))
        l_test = torch.tensor(list(test_data[i]["length"]))

        train_set = PepDS(X_train, Y_train, l_train)
        val_set   = PepDS(X_val,   Y_val,   l_val)
        test_set  = PepDS(X_test,  Y_test,  l_test)

        train_dl = DataLoader(train_set, batch_size=batch_size, shuffle=True)
        val_dl   = DataLoader(val_set)
        test_dl  = DataLoader(test_set)

        bp = {"train_epoch": 0, "val_epoch": 0, "train_RMSE": 9e8, "val_RMSE": 9e8, "val_R2": -9e8, "test_RMSE": 9e8, "test_R2": -9e8}
        results.append(bp)
        train_single(model, train_dl, val_dl, test_dl, results[i], lr = lr, fold = i+1, epochs = epochs)        
        del model
    timeStamp = datetime.now().strftime("%Y-%m-%d__%H-%M-%S")
    with open(f"logs/{timeStamp}__{k}-fold_cross_validation.txt", 'w') as f:
        f.write(model_summary)
        f.write(f"\n\nNumber of LSTM layers = {num_layers}, Embedding dim = {embedding_dim}, hd1 = {hidden_dim1}, hd2 = {hidden_dim2}\n")
        f.write(f"Dropout coefficient = {dr_o}, batch size = {batch_size}, learning rate = {lr}\n\n")
        for i, r in enumerate(results):
            f.write(f"Fold {i+1}:\n")
            f.write(str(r)+"\n\n")    
######################################################################################################
######################################################################################################
def train_single (model, train_dl, val_dl, test_dl, best_results, lr, fold, epochs = 1000):
    parameters = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = torch.optim.Adam(parameters, lr=lr)
    saved_model_file = ""
    for i in range(epochs):
        model.train()
        sum_loss = 0.0
        total = 0
        for x, y, l in train_dl:
            x = x.long()
            y = y.float()
            optimizer.zero_grad()
            y_pred = model(x,l)
            #print(y_pred)
            loss = F.mse_loss(y_pred, y.unsqueeze(-1))
            loss.backward()
            optimizer.step()
            sum_loss += loss.item()*y.shape[0]
            total += y.shape[0]
        train_RMSE = np.sqrt(sum_loss/total)
        val_R2, val_RMSE = validation(model, val_dl)
        if train_RMSE < best_results["train_RMSE"]:
            best_results["train_epoch"] = i
            best_results["train_RMSE"] = train_RMSE
        if val_RMSE < best_results["val_RMSE"]:
            for old in glob.glob(f"saved_models/fold_{fold}--*"):   os.remove(old)
            saved_model_file = f"saved_models/fold_{fold}--epoch_{i+1}--val_R2_{val_R2:.3f}.pt" 
            torch.save(model, saved_model_file)
            best_results["val_epoch"] = i
            best_results["val_RMSE"] = val_RMSE
            best_results["val_R2"] = val_R2
        print(f"Fold {fold}  Epoch {i+1:<4d} Train RMSE =  {train_RMSE:.3f}  Val RMSE = {val_RMSE:.3f}   Val_R2 = {val_R2:.3f}")
        if (i - best_results["train_epoch"] >20) and (i - best_results["val_epoch"] >20):        
            break
    best_model = torch.load(saved_model_file) 
    best_results["test_R2"]  , best_results["test_RMSE"] = validation(best_model, test_dl)
    print(best_results)
    del best_model
#######################################################################################################
#######################################################################################################
def validation (model, dl):
    model.eval()
    correct = 0
    total = 0
    sum_loss = 0.0
    Y = []
    Y_hat = []
    for x, y, l in dl:
        x = x.long()
        y = y.float()
        Y.append(y)
        y_hat = model(x, l)
        #import pdb; pdb.set_trace()
        Y_hat.append(y_hat)
    return r2_score(Y,Y_hat) , np.sqrt(mean_squared_error(Y_hat, Y))
#######################################################################################################
#######################################################################################################
######################################################################################################
#######################################################################################################


AA, data = prepareData("03__Salmonella__clean_convert.csv")
data["encoded_FASTA"] = data["FASTA"].apply(AA.FASTA2indices)
data["length"] = data["FASTA"].apply(len)
data.dropna(axis = 0, inplace = True)
data.reset_index(inplace = True, drop = True)
Path("saved_models").mkdir(parents=True, exist_ok=True)
Path("logs").mkdir(parents=True, exist_ok=True)

train_k_fold(data, num_layers = 2 , embedding_dim = 38, hidden_dim1 = 16 , hidden_dim2 = 37, dr_o = 0.6, k =8, epochs = 1000, lr=0.003, batch_size = 10)

