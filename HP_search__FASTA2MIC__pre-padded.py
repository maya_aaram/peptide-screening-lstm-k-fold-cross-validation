# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function, division
from io import open
import unicodedata
import string
import re
import random

import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F
from torch.utils.data import TensorDataset , DataLoader
import pandas as pd
import numpy as np
from sklearn.metrics import r2_score , mean_squared_error
import matplotlib.pyplot as plt

from pyGPGO.covfunc import matern32
from pyGPGO.acquisition import Acquisition
from pyGPGO.surrogates.GaussianProcess import GaussianProcess
from pyGPGO.GPGO import GPGO

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
epochs_per_evaluation = 1000

log_file = "Hyper_parameter_optimization__pre_padded.log"

class AAs:
    def __init__(self, name, sequences):
        # The constructor expects a name and a list of strings.
        self.name = name
        self.aa2index = {}
        self.aa2count = {}
        self.index2aa = {}
        self.n_residues = 0  # start as empty dict
        print("Counting amino acids ...")
        for seq in sequences:
            self.addSequence(seq)

    def addSequence(self, sequence):
        for aa in list(sequence.upper()):
            self.addResidue(aa)

    def addResidue(self, aa):
        if aa not in self.aa2index:
            self.aa2index[aa] = self.n_residues + 1
            self.aa2count[aa] = 1
            self.index2aa[self.n_residues] = aa
            self.n_residues += 1
        else:
            self.aa2count[aa] += 1
    
    def FASTA2indices(self, seq, max_length = 70):
        indices = [0] * max_length
        l = len(seq)
        try:
            for i, aa in enumerate(list(seq.upper())):
                indices[max_length-l+i] = self.aa2index[aa]
            return indices
        except:
            print(f"Something didn't work when converting {seq} to indices.")
            return None

######################################################################
def prepareData(filename):
    pairs = pd.read_csv(filename)
    AA = AAs("Amino Acids", pairs["FASTA"].tolist())
    print("Counted amino acids:", AA.name, AA.n_residues)
    return AA, pairs
######################################################################################################
######################################################################################################
def train_model_regr(embedding_dim, n_layers, hd1, hd2, dropout, lr= 2):
    
    # Explicitly cast int variables, because PyGPGo doesn't seem to respect int types.
    model =  LSTM_regr(vocab_size = 21, embedding_dim = int(embedding_dim), n_layers = int(n_layers),
                hidden_dim1 = int(hd1), hidden_dim2 = int(hd2), dr_o = dropout)
    parameters = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = torch.optim.Adam(parameters, lr=10**(-lr))
    print(f"Embedding dim = {embedding_dim}\tN_LSTM_layers = {n_layers}\thd1 = {hd1}\thd2 = {hd2}\tDropout = {dropout}\teps = {10**(-lr)}\n")
    delta = 0.05 
    best_param ={}                                                                                                      
    best_param["train_epoch"] = 0                                                                                       
    best_param["val_epoch"] = 0                                                                                         
    best_param["train_MSE"] = 9e8                                                                                       
    best_param["val_RMSE"] = 9e8 
    best_param["val_R2"] = -9e8 

    for i in range(epochs_per_evaluation):
        model.train()
        sum_loss = 0.0
        total = 0
        for x, y in train_dl:
            x = x.long()
            y = y.float()
            y_pred = model(x)
            #print(y_pred)
            optimizer.zero_grad()
            loss = F.mse_loss(y_pred, y.unsqueeze(-1))
            loss.backward()
            optimizer.step()
            sum_loss += loss.item()*y.shape[0]
            total += y.shape[0]
        #train_loss_list.append(sum_loss/total)
        train_MSE = sum_loss/total
        val_R2, val_RMSE = validation_metrics_regr(model, val_dl)
        #print(f"Epoch {i} Training MSE: {train_MSE:.1f}  Validation RMSE: {val_RMSE:.1f}  Validation R2: {val_R2:.2f}")
        if train_MSE < (best_param["train_MSE"] - delta):
            best_param["train_epoch"] = i
            best_param["train_MSE"] = train_MSE
        if val_RMSE < (best_param["val_RMSE"] - delta):
            best_param["val_epoch"] = i
            best_param["val_RMSE"] = val_RMSE
            best_param["val_R2"] = val_R2
        print(i, best_param)
        if (i - best_param["train_epoch"] >10) and (i - best_param["val_epoch"] >10):
            break
    test_MSE = best_param["train_MSE"]
    val_RMSE = best_param["val_RMSE"]
    val_R2 = best_param["val_R2"]

    with open(log_file,'a') as f:
        f.write(f"Embedding dimensions: {embedding_dim}, Hidden dimensions 1: {hd1}, Hidden dimension 2: {hd2}, ")
        f.write(f"Dropout: {dropout:.3f}, learning rate: {10**(-lr):.4f} ")
        f.write(f"Best test MSE: {test_MSE:.1f},  Best Val RMSE: {val_RMSE:.1f},  Best Val R2: {val_R2:.3f}\n")
    print(best_param["val_R2"])
    del model
    return best_param["val_R2"]
'''
    del model
	# GPGO maximize performance by default, set performance to its negative value for minimization
    if direction:
        return best_param["test_MSE"]
    else:
        return -best_param["test_MSE"]

        print("Train MSE =  %.3f  Val RMSE = %.3f   Val_R2 = %.3f" % (sum_loss/total, val_rmse, val_r2))

        if i % 1 == 0:
            #import pdb; pdb.set_trace()
            plt.plot(np.arange(i+1), train_loss_list, label="train")
            plt.plot(np.arange(i+1), val_r2_list, label="val_r2")
            plt.plot(np.arange(i+1), val_rmse_list, label="val rmse")
            plt.legend()
            plt.savefig('plots/c.png', bbox_inches='tight', pad_inches=0.1, dpi = 300)
            plt.clf()
            plt.close()
'''

#######################################################################################################
#######################################################################################################
def validation_metrics_regr (model, valid_dl):
    model.eval()
    correct = 0
    total = 0
    sum_loss = 0.0
    Y = []
    Y_hat = []
    for x, y in valid_dl:
        x = x.long()
        y = y.float()
        Y.append(y)
        y_hat = model(x)
        #import pdb; pdb.set_trace()
        Y_hat.append(y_hat)
    return r2_score(Y,Y_hat) , np.sqrt(mean_squared_error(Y_hat, Y))
#######################################################################################################
#######################################################################################################
class LSTM_regr(torch.nn.Module) :
    def __init__(self, vocab_size, embedding_dim, n_layers, hidden_dim1, hidden_dim2, dr_o = 0.1, batch_first=True ) :
        super().__init__()
        #print(f"\n\n\n{embedding_dim}\t{hidden_dim1}\t{hidden_dim2}\n\n\n")
        self.embeddings = nn.Embedding(vocab_size, embedding_dim, padding_idx=0)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim1, num_layers = n_layers, batch_first=True)
        self.linear1 = nn.Linear(hidden_dim1, hidden_dim2)
        self.linear2 = nn.Linear(hidden_dim2, 1)
        self.dropout = nn.Dropout(dr_o)
        
    def forward(self, x):
        x = self.embeddings(x)
        lstm_out, (ht, ct) = self.lstm(x)
        x= F.relu(self.linear1(ht[-1]))
        x = self.dropout(x)
        return self.linear2(x)
#######################################################################################################
#######################################################################################################

AA, data = prepareData("03__Salmonella__clean_convert.csv")
data["encoded_FASTA"] = data["FASTA"].apply(AA.FASTA2indices)
data.dropna(axis = 0, inplace = True)
data.reset_index(inplace = True, drop = True)

train_data = data.sample(frac=0.85, axis = 0, random_state = 88)
val_data = data[~data.index.isin(train_data.index)]

train_data.reset_index(inplace = True, drop = True)
val_data.reset_index(inplace = True, drop = True)

X_train = torch.tensor(list(train_data["encoded_FASTA"]))
Y_train = torch.tensor(list(train_data["MIC_against_Salmonella(µM)"]))

X_val = torch.tensor(list(val_data["encoded_FASTA"]))
Y_val = torch.tensor(list(val_data["MIC_against_Salmonella(µM)"]))

train_set = TensorDataset(X_train, Y_train)
val_set = TensorDataset(X_val, Y_val)

train_dl = DataLoader(train_set, batch_size=10, shuffle=True)
val_dl = DataLoader(val_set)



cov = matern32()
gp = GaussianProcess(cov)
acq = Acquisition(mode='UCB')
#acq = Acquisition(mode='ExpectedImprovement')
param = {
		'embedding_dim': ('int', [2, 50]),
		'n_layers': ('int', [1, 4]),
		'hd1': ('int', [10, 50]),
		'hd2': ('int', [4, 50]),
		'dropout': ('cont', [0.00001, 0.6]),
		'lr': ('cont', [1, 4])
		}
#np.random.seed(168)
gpgo = GPGO(gp, acq, train_model_regr, param)
gpgo.run(max_iter=5000,init_evals=3)

